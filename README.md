# TMGMT Translator LibreTranslate

LibreTranslate translator plugin for the Translation Management 
Tools (TMGMT) project. Allows to use machine translation provided by 
LibreTranslate to translate content.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tmgmt_libretranslate).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tmgmt_libretranslate).


## Table of contents

- Requirements
- Installation
- Features
- Configuration
- Maintainers


## Requirements

- Depends on Translation Management Tools (http://drupal.org/project/tmgmt).
- Registration on https://libretranslate.com/ is required to use this module or using a
  self-hosted server (https://github.com/LibreTranslate/LibreTranslate).


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Features

- Fast automated translation of content using LibreTranslate Translate Service
  `https://github.com/LibreTranslate/LibreTranslate`
- Translate one or multiple nodes by a few simple mouse clicks
- Use advanced translation jobs management tool to submit and review translations


## Configuration

- Get an api key on `https://portal.libretranslate.com/` or you can self
  host the translation API `https://github.com/LibreTranslate/LibreTranslate`
- Go to `/admin/tmgmt/translators` and configure the provider apikey and URL.


## Maintainers

- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)


This project has been sponsored by:
- Visit: [Javali](https://www.javali.pt) for more information